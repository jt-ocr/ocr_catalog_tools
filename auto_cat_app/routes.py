# import os
from flask import Flask, request, url_for, render_template, redirect, send_file
from werkzeug import secure_filename
import catalog_utility, path_ref_root
import os

root_path = os.path.dirname(path_ref_root.__file__)
UPLOAD_FOLDER = os.path.join(root_path, 'uploaded_catalog')
RESULTS_PATH = os.path.join(root_path, 'catalog_results')
ALLOWED_EXTENSIONS = set(['csv'])

mh = catalog_utility.needs_review_config().master_hierarchy

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_filename(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_filename(file.filename):
            print(root_path,'\t',UPLOAD_FOLDER,'\t',RESULTS_PATH)
            client_number = file.filename.split('-')[0]
            print(client_number)
            catalog_path = os.path.join(UPLOAD_FOLDER, client_number+'.csv')
            print(catalog_path)
            ignore_system_warnings = request.form['ignore_system_warnings']
            print(ignore_system_warnings)
            file.save(catalog_path)
            result_tuple = catalog_utility.run_cat_run(client_number, catalog_path, RESULTS_PATH, ignore_system_warnings)
            if result_tuple[0] == 0:
                return send_file(result_tuple[1], as_attachment=True, mimetype="text/csv", attachment_filename="%s_problem_children.csv" % client_number)
            elif result_tuple[0] == 1:
                return send_file(result_tuple[1], as_attachment=True, mimetype="text/csv", attachment_filename="TEST_%s_Catalog.csv" % client_number)
        else:
            return render_template('index.html')
    elif request.method == 'GET':
        return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
