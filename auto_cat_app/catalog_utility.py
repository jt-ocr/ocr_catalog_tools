"""

# Following is a list of objects such as dictionaries and lists that are needed for
  various subroutines of this application.

#  hierarchy was intended as a multi-layered dictionary used as a reference for unique value-sets that should be associated with a given set of categorizations
   e.g. '4Moms' and 'Rockaroo' brands should only ever be recommended for '4Moms' in the 'manufacturer' column

#  countData is an array of dictionaries that is initialized at the beginning of runHierarchy_click
   It has as many elements as there are corresponding columns in the hierarchy. Each index is a dictionary consisting of alphanumeric keys
   which are in turn dictionaries of subset groups within the category population. The result is a count of how many times a group (e.g. 'Y', 'Z-Exclude')
   is associated with a given characteristic (i.e. alphanumeric key)
   E.g. within the Tracked ASIN category, 4 ASINs were Z-Exclude, 10 ASINs were Y where the word "bleep" appeared in the Title (or A+ content etc.)

#  cellX holds a set of unique words in each title minus special characters (and non-value-add words)

#  metaData[_needs_review] will hold meta data for various algorithms. This container will be used to optimize algorithm selection and performance.

#  autoCat hold the auto-classified suggestions`

#  numeratorCat and denominatorCat will hold count data for maxKey values for a single ASIN at a time - used in calculating ratios used to determine Categorization Value

#  tempKeyRatios will aggregate data from numeratorCat and denominatorCat

#  needsReviews will hold all the ASINs (with titles/text) to be categorized

"""

import sys
import json
import time
import csv
import requests
# from nltk.tokenize import word_tokenize
import pickle
# import string


# time_stamps['Kickoff'] = time.time()
# print('Kickoff: ', time.ctime(time_stamps['Kickoff']))
class needs_review_config:
    noiseWords = {"and", "a", "to", "of", "in", "is", "the", "that", "it", "on", "this", "for", "but", "with", "are", "have", "be", "at", "per", "or", "as", "was", ""}
        # if measurements == True:
            # self.noiseWords += {"pack", "-pack", "packs", "size", "packet", "packets", "bag", "bags"
            # , "count", "-count", "piece", "-piece", "pieces", "pouch", "pouches", "package", "packages", "box", "boxes"
            # , "inch", "inches", "ounce", "-ounce", "ounces", "oz", "-oz", "pound", "-pound", "pounds", "lb", "-lb", "lbs", "ml"}
    SpecialCharacters = "!||@||&amp||&quot||¢||â||é||š||¬||¼||¾||&||$||€||^||~||-||+||_||=||<||>||`||'||*||¨||(||)||{||[||]||}||™||®||©||Ã||Â||Å||ƒ||,||?||:||;||/||\||| ||...||..||.||%||¶||�||\\n||\\t||\\r".split(sep='||')+list(chr(i) for i in range(32))+list(chr(34))+list(chr(46))+list(chr(i) for i in range(124,5000))
    params = {
                'currentHierarchy': []
            }
    # path = '\\\\192.168.8.88\\OCRMainShare\\Client Team\\OCR_Main_Share\\zDocuments\\Automation Tools\\AutoCat\\hierarchy_prod.json'
    path = 'hierarchy_prod.json'
    with open(path,'r') as f:
        h = json.load(f)
    master_hierarchy = h


# LEVENSHTEIN DISTANCE:
def levenshtein_distance(first, second):
    """Find the Levenshtein distance between two strings."""
    if len(first) > len(second):
        first, second = second, first
    if len(second) == 0:
        return len(first)
    first_length = len(first) + 1
    second_length = len(second) + 1
    distance_matrix = [[0] * second_length for x in range(first_length)]
    for i in range(first_length):
        distance_matrix[i][0] = i
    for j in range(second_length):
        distance_matrix[0][j] = j
    for i in range(1, first_length):
        for j in range(1, second_length):
            deletion = distance_matrix[i-1][j] + 1
            insertion = distance_matrix[i][j-1] + 1
            substitution = distance_matrix[i-1][j-1]
            if first[i-1] != second[j-1]:
                substitution += 1
            distance_matrix[i][j] = min(insertion, deletion, substitution)
    return distance_matrix[first_length-1][second_length-1]

def extract_hierarchy_set(x):
    l = set()
    print("Extracting hierarchy set with initialized output of ", l)
    if type(x) is list:
        for i in x:
            if type(i) is int:
                l.add(i)
            else:
                extract_hierarchy_set(i)
    else:
        l.add(i)
    if -1 in l:
        l.remove(-1)
    return l

def extract_hierarchy_list(x):
    l = list()
    print("Extracting hierarchy list with initialized output of ", l)
    for i in x:
        if i[1] not in l:
            l.append(i[1])
        else:
            raise ValueError("ERROR: You cannot have multiple parent elements for a given classification. Parents may only have a 1-to-1 OR 1-to-many relationship with sub-classifications - NEVER a many-to-1. Tell the douchebag that screwed it up to fix it.")
    return l


# def get_catalog_api(master_hierarchy, h):
#     api = 'https://api.oneclickretail.com/v2/clients/%s/download_catalog?X-API-KEY=%s' % (master_hierarchy[h]['uuid'], master_hierarchy[h]['api_key'])
#     return api

# def get_catalog(url):
#     with requests.Session() as s:
#         print('Retrieving catalog.', end = '')
#         downloadc = s.get(url)
#         print('.',end='')
#         contentc = downloadc.content.decode(errors = 'replace')
#         print('.',end='')
#         cat = csv.reader(contentc.splitlines(), delimiter = ',', skipinitialspace = True)
#         print('.',end='')
#         catalog = list(cat)
#         print('.',end='')
#         return catalog

# def get_catalog_api(filepath):
#     api = filepath
#     return api

def get_catalog(filepath):
    with open(filepath, 'r', errors="ignore") as s:
        print('Retrieving catalog.', end='')
        cat = csv.reader(s, delimiter=',', skipinitialspace=True)
        print('.', end='')
        catalog = list(cat)
        print('.', end='')
        return catalog

def isnum(s):
    try:
        float(s)
        return True
    except:
        try:
            if s.isnumeric():
                return True
        except:
            return False

def run_cat_run(client_num, catalog_path, results_path, ignore_system_warnings):
   # >>>>
    time_stamps = {}
    hierarchy = []
    catalogRef = []
    catalogRef_needs_review = []
    hierarchy_problem_children = {}
    lev_dist_problem_children = {}
    absolutely_nots = None
    countData = []
    # count_words = {}
    cellX = []
    cellX_needs_review = []
    unique_words = []
    autoCat = []
    autoCat_needs_review = []
    new_words = []

    numeratorCat = {}
    denominatorCat = {}
    tempKeyRatios = {}
    tempKeyTie = {}
    maxWord = ''
    maxWordVal = 0
    maxWordCount = 0
    minWord = ''
    minWordVal = 0
    maxVal = 0
    maxKey = ''
    sumVal = 0
    metaData = {}
    cellY = []
    nw_set = {}
    flag_nw_set = 0
    temp = []
    # cellY2 = []
    # options = []
    # accuracy_tracker = []
    # best_method = []
    flag_bucket = {}
    categorization_problem_children = {}
    output_needs_review = {}
    # Assign configurations for the current file. Under class 'needs_review_config' the 'categorizer_params' method holds, among other things, relevant column numbers
    word_phrase_length = 2
    parms = None
    parms = needs_review_config()
    master_hierarchy = parms.master_hierarchy

    # Populate @catalogRef
    #     Remember that @catalogRef holds a mirror image of the catalog. We will only be including NON-NEEDS REVIEW items. NEEDS REVIEW items will be put in a different list
    # Get the hierarchy column set and initialize the '@push' variable by adding a column for the ASIN (index 0), the Title (index n), and each unique column in the hierarchy (index 1 to n-1)

    h = client_num

    if h not in master_hierarchy.keys():
        sys.exit("This client ID has not been configured. Please update the hieararchy json file and try again.")

    # for h in master_hierarchy.keys():
    time_stamps[h+'-Start'] = time.time()
    print('\n\nBeginning process for client ', master_hierarchy[h]["client_name"], '\t', time.ctime(time_stamps[h+'-Start']))

    cols = extract_hierarchy_list(master_hierarchy[h]['hierarchy'])
    h_cols = master_hierarchy[h]['hierarchy']
    title = master_hierarchy[h]['titleCol']
    asin = master_hierarchy[h]['ASINcol']
    trackedCol = master_hierarchy[h]['hierarchy'][0][1]
    pValue = master_hierarchy[h]['pValue']

    # Get the catalog
    catalog = get_catalog(catalog_path)
    time_stamps[h + '-catalog'] = time.time()
    print('Success! ', time.ctime(time_stamps[h + '-catalog']))

    # Construct a hierarchy with the catalog you just got
    print('Constructing hierarchy[', end='')
    for i in cols:
        hierarchy.append({})
    # catalogRef holds a mirror of the catalog in its current state with only the relevant columns in the form: [ASIN, <hierarchy cols>, Title]
    icnt = 1
    for i in catalog[1:]:
        if icnt % round(len(catalog) / 10) == 0:
            print('.', end='')
        push = [None] * (len(cols) + 2)
        if i[trackedCol].lower().find('need') != -1:
            push[0], push[-1] = str(i[asin]), str(i[title])
            catalogRef_needs_review.append(push)
            push = [None] * (len(cols) + 2)
            push[0], push[-1] = str(i[asin]), str(i[title])
            autoCat_needs_review.append(push)
        elif i[trackedCol].lower().find('need') == -1:
            push[0], push[-1] = str(i[asin]), str(i[title])
            cri = 1
            fchar = str(i[trackedCol][0].lower())
            for k in cols:
                p_col = h_cols[cri-1][0]
                push[cri] = str(i[k])
                # If this is a tracked ASIN, I want to add the appropriate elements to the hierarchy
                if fchar == 'y' and cri == 1:
                    # This unnecessary repition could have been avoided - but I thought of it after I wrote a bunch more code and didn't want to have to bother checking whether it'd screw anything up to make the changes necessary to not repeat it... but functionally it just ensures that we don't have any "Y - Dormant" garbage in here
                    hierarchy[0]['Y'] = 0
                elif fchar == 'y':
                    # Check to make sure that all the values are the same - if there are inconsistencies (i.e. multiple parents per child) add those results to the hierarchy_problem_children dictionary
                    if i[k] not in hierarchy[cri-1] and i[k] != '' and p_col == -1:
                        # print('Adding New Parent:   ', i[k], '   at hierarchy index   ', cri-1, '   from column   ', k)
                        hierarchy[cri-1][i[k]] = 0
                    elif i[k] not in hierarchy[cri-1] and i[k] != '' and p_col != -1:
                        # print('Adding New Child:   ', i[k], '   at hierarchy index   ', cri-1, '   from column   ', k,'.\t|\tParent Item/Column: ', i[p_col],'/',p_col)
                        hierarchy[cri-1][i[k]] = str(i[p_col])
                    elif i[k] in hierarchy[cri-1] and hierarchy[cri-1][i[k]] != i[p_col] and p_col != -1:
                        pc_code = str(i[k])+' - '+str(k)
                        if pc_code not in hierarchy_problem_children:
                            hierarchy_problem_children[pc_code] = set()
                            hierarchy_problem_children[pc_code].add(hierarchy[cri-1][i[k]]+' - '+str(p_col))
                            hierarchy_problem_children[pc_code].add(str(i[p_col])+' - '+str(p_col))
                        elif pc_code in hierarchy_problem_children:
                            hierarchy_problem_children[pc_code].add(str(i[p_col])+' - '+str(p_col))
                cri += 1
            catalogRef.append(push)
            push = [None]*(len(cols)+2)
            push[0], push[-1] = str(i[asin]), str(i[title])
            autoCat.append(push)
        icnt += 1
    time_stamps[h + '-hierarchy'] = time.time()
    print(']  Success!\t', time.ctime(time_stamps[h + '-hierarchy']))
    if len(hierarchy_problem_children) > 0 and ignore_system_warnings == 'False':
        time_stamps[h + '-problem_children'] = time.time()
        print('\n\nYour catalog has issues. Please check the following in client #', h, "'s catalog:\n\n", hierarchy_problem_children)
        phc_outf = ''
        for ix in hierarchy_problem_children:
            phc_outf += "\n" + ix
            for iz in hierarchy_problem_children[ix]:
                phc_outf += "\n," + iz
        phc_outfile_name = results_path + "\\meta_data\\%s-%s-hierarchy_problem_children.csv" % (h, master_hierarchy[h]["client_name"])
        with open(phc_outfile_name, "w") as f:
            for zz in range(len(phc_outf)):
                try:
                    f.write(phc_outf[zz])
                except:
                    f.write(bytes('[?]',"utf-8"))
            return (0, phc_outfile_name)
        # print("%s-hierarchy_problem_children.csv created" % h)
        # time.sleep(1)
        # print("Moving on in ", end='')
        # for i in range(3, 0, -1):
        #     print(str(i)+' ', end='')
        #     time.sleep(1)
        #     catalogRef_needs_review.clear()
        #     autoCat_needs_review.clear()
        #     hierarchy.clear()
        #     hierarchy_problem_children.clear()
        #     catalogRef.clear()
        #     autoCat.clear()
        # continue

    metaData['problem_children'] = hierarchy_problem_children

    # Do a quick analysis on potential misspellings




    categorization_problem_children = {}
    tempset1 = set()
    tempset2 = set()
    for i in output_needs_review:
    # check for literal difference and for relative difference
        input()





    # COUNT DATA
    #   Start off by cleaning the ASIN titles and passing the parsed string into cellX[]
    cnt = 0

    print('\nCleaning catalogRef', end='')

    absolutely_nots = "!||@||&amp||&quot||¢||â||é||š||¬||¼||¾||&||$||€||^||~||- || -||+||_||=||<||>||`||'||*||¨||(||)||{||[||]||}||™||®||©||Ã||Â||Å||ƒ||,||?||:||;|| /||/ ||\||| ||...||..||%||¶||�||\\n||\\t||\\r".split(sep='||')
    single_absolutely_nots = "-||".split(sep='||')
    check_numeric = str.maketrans('./', '  ')

    icnt=1
    for row in catalogRef:
        if icnt % round(len(catalogRef)/10) == 0:
            print('.', end='')
        cellX.append(row[-1].lower())
        # Remove special characters
        for char in parms.SpecialCharacters:
            kc = cellX[-1].find(char)
            if kc == -1:
                continue
            for occur in range(cellX[-1].count(char)):
                kc = cellX[-1].find(char)
                if kc == 0:
                    cellX[-1] = cellX[-1].replace(char, '', 1)
                elif kc != -1 and char != chr(34) and not cellX[-1][kc - 1].isnumeric():
                    if kc == ',':
                        cellX[-1] = cellX[-1].replace(char, ' ', 1)
                    else:
                        cellX[-1] = cellX[-1].replace(char, '', 1)
                elif cellX[-1][kc-1].isnumeric() and char in ('.'):
                    pass
                elif (cellX[-1][kc-1].isnumeric() or cellX[-1][kc-1] == ' ') and char in ('/', '-', '(', ')'):
                    cellX[-1] = cellX[-1].replace(char, ' ', 1)
                elif kc == chr(34):
                    if (cellX[-1][kc-1] in parms.SpecialCharacters):
                        cellX[-1] = cellX[-1].replace(char, ' ', 1)
                    break
        # Create word phrases N words in length (N being 'word_phrase_length')
        temp_ii=set()
        for foo in range(len(cellX[-1].strip().split()[:-word_phrase_length + 1])):
            temp = []
            for ba in range(word_phrase_length):
                temp.append(cellX[-1].strip().split()[foo + ba])
            if (
                # (not temp[0].isnumeric() and temp[1].isnumeric()) or
                # (temp[0].isnumeric() and not temp[1].isnumeric()) or
                (not temp[0].isnumeric() and not temp[1].isnumeric())
                ) and (not temp[0] in parms.noiseWords and not temp[1] in parms.noiseWords):
                temp_ii.add(' '.join(temp))
        cellX[-1] = set(cellX[-1].strip().split()) - parms.noiseWords
        temp_i = set()
        for i in cellX[-1]:
            i = i.strip()
            if i not in absolutely_nots:
                for char in absolutely_nots:
                    i = i.replace(char, '')
                if len(str(i.strip())) > 1:
                    temp_i.add(str(i.strip()))
        # # <!> If order ends up mattering, you'll need to go up about 10 lines and add the quasi-permutation there, before cellX_needs_review[-1] becomes a set...
        # # This will create an N choose 2 combination of words
        # temp_ii = list(temp_i)
        # for foo in temp_ii[:-1]:
        #     for ba in temp_ii[temp_ii.index(foo)+1:]:
        #         temp_i.add(tuple(sorted((str(foo), str(ba)))))
        cellX[-1] = set(temp_i | temp_ii)
        icnt += 1
        # Here I may wish to add logic that tests the value of measurement words like 'pack' and 'ounce'. If such words are evenly distributed across categorization groups, we could do without them.
    # Now do the same thing for the needs review items.
    icnt=1
    for row in catalogRef_needs_review:
        if icnt % round(len(catalogRef_needs_review)/10) == 0:
            print('.', end='')
        cellX_needs_review.append(row[-1].lower())
        # Remove special characters
        for char in parms.SpecialCharacters:
            kc = cellX_needs_review[-1].find(char)
            if kc == -1:
                continue
            for occur in range(cellX_needs_review[-1].count(char)):
                kc = cellX_needs_review[-1].find(char)
                # If char is the first character of the title...
                if kc == 0:
                    cellX_needs_review[-1] = cellX_needs_review[-1].replace(char, '', 1)
                # If char is present and not a space and is a Letter...
                elif kc != -1 and char != chr(34) and not cellX_needs_review[-1][kc - 1].isnumeric():
                    cellX_needs_review[-1] = cellX_needs_review[-1].replace(char, '', 1)
                # If the character before char is a number...
                elif cellX_needs_review[-1][kc-1].isnumeric() and char in ('.'):
                    pass
                # If the character before char is a number or a space and the char is / or - then replace it with a space.
                elif (cellX_needs_review[-1][kc-1].isnumeric() or cellX_needs_review[-1][kc-1] == ' ') and char in ('/', '-'):
                    cellX_needs_review[-1] = cellX_needs_review[-1].replace(char, ' ', 1)
                # This is kind of just here out of a superstitious whim...
                elif kc == chr(34):
                    if (cellX_needs_review[-1][kc-1] in parms.SpecialCharacters):
                        cellX_needs_review[-1] = cellX_needs_review[-1].replace(char, ' ', 1)
                    break
        # Create word phrases N words in length (N being 'word_phrase_length')
        temp_ii=set()
        for foo in range(len(cellX_needs_review[-1].strip().split()[:-word_phrase_length + 1])):
            temp = []
            for ba in range(word_phrase_length):
                temp.append(cellX_needs_review[-1].strip().split()[foo + ba])
            if (
                # (not temp[0].isnumeric() and temp[1].isnumeric()) or
                # (temp[0].isnumeric() and not temp[1].isnumeric()) or
                (not temp[0].isnumeric() and not temp[1].isnumeric())
                ) and (not temp[0] in parms.noiseWords and not temp[1] in parms.noiseWords):
                temp_ii.add(' '.join(temp))
        cellX_needs_review[-1] = set(cellX_needs_review[-1].strip().split()) - parms.noiseWords
        temp_i = set()
        for i in cellX_needs_review[-1]:
            i = i.strip()
            if i not in absolutely_nots:
                for char in absolutely_nots:
                    i = i.replace(char, '')
                if len(str(i.strip())) > 1:
                    temp_i.add(str(i.strip()))
        # # <!> If order ends up mattering, you'll need to go up about 10 lines and add the quasi-permutation there, before cellX_needs_review[-1] becomes a set...
        # # This will create an N choose 2 combination of words
        # temp_ii = list(temp_i)
        # for foo in temp_ii[:-1]:
        #     for ba in temp_ii[temp_ii.index(foo)+1:]:
        #         temp_i.add(tuple(sorted((str(foo), str(ba)))))
        cellX_needs_review[-1] = set(temp_i | temp_ii)
        icnt += 1
        # for i in cellX_needs_review[-1]:
        #     cellX_needs_review[-1].remove(i)
        #     if cellX_needs_review[-1].add(i.strip()) not in absolutely_nots:
        #         cellX_needs_review[-1].add(i.strip())



    print('\nCounting Data...')

    r = 0
    for c in cols:
        countData.append({})
    for row in catalogRef:
        for c in range(1, len(cols)+1):
            for key in cellX[r]:
                if row[c] == '':
                    continue
                # elif (c > 1 and row[c][0].lower() == 'z'):
                #     continue
                if key not in countData[c-1] and key is not None:
                    countData[c-1][key] = {}
                # For the Tracked Item column, we only care about "Y", "Y-Passthrough", "N-Not tracked category", and "Z-Exclude"
                # >> Thus, only look at the FIRST LETTER for the tracked item column.
                # We also only want counts to go toward relevant categorizations
                # >> Thus, only categorizations in columns subsequent to a Y-tracked item will have count data attributed to them
                if c == trackedCol:
                    if row[c][0].lower() == "y":
                        if 'pass' in row[c].lower():
                            row[c] = "y - passthrough".upper()
                        else:
                            row[c] = "y".upper()
                    elif row[c][0].lower() == "n":
                        row[c] = "n-not tracked category".upper()
                    elif row[c][0].lower() == "z":
                        row[c] = "z-exclude".upper()
                    if row[c] not in countData[c-1][key] and row[c] is not None:
                        countData[c-1][key][row[c]] = 1
                    elif row[c] is not None:
                        countData[c-1][key][row[c]] += 1
                elif c > 1 and row[trackedCol][0].lower() == "y":
                    if row[c] not in countData[c-1][key] and row[c] is not None:
                        countData[c-1][key][row[c]] = 1
                    elif row[c] is not None:
                        countData[c-1][key][row[c]] += 1
        r += 1
    time_stamps[h+'-count'] = time.time()
    print('Count Data Complete. ', time.ctime(time_stamps[h+'-count']))

# #Begin AutoCat...
    # Every automated decision option is recorded in autoCat[_needs_review]. A dictionary after the form:
    #       {<classification> : {<method of classification used> : <meta data about that decision>}
    #       meta data will typically be a list ending with some type of decimal value... don't ask. just read the code...

    # RUN "Unique Key-Words"
    #   unique_words finds all the words that are only ever associated with a single categorization and then a title's word set is then compared to each categoriation's word set.
    #   Get single valued sets
    for i in range(len(countData)):
        unique_words.append({})
        for k in countData[i].keys():
            if len(countData[i][k].items()) == 1 and isnum(k) is not True:
                for l in countData[i][k].keys():
                    if l not in unique_words[i]:
                        unique_words[i][l] = set()
                        unique_words[i][l].add(k)
                    else:
                        unique_words[i][l].add(k)
    for r in range(len(catalogRef_needs_review)):
        for c in range(len(countData)):
            # remember that 'i' is the calssification (e.g. 'Z-EXCLUDE', 'N-NOT TRACKED' etc.) - not a keyword ('ball', 'spray', etc.)
            for i in unique_words[c].keys():
                if len(unique_words[c][i] & cellX_needs_review[r]) > 0:
                    # catalogRef_needs_review[r][c+1] = i
                    if autoCat_needs_review[r][c+1] is None:
                        # Every automated decision option is recorded in autoCat[_needs_review]. A dictionary after the form:
                        #       {<classification> : {<method of classification used> : <meta data about that decision>}}
                        #   Here, in unique_words, the meta data includes a list containing (a)the inner join set of the flagged words for the automated decision and the title of the item and (b)the ratio of words in the joined set / total words in the title.
                        autoCat_needs_review[r][c+1] = {}
                        autoCat_needs_review[r][c+1][i] = {'unique_words': [unique_words[c][i] & cellX_needs_review[r], sum([countData[c][word][i] for word in unique_words[c][i] & cellX_needs_review[r]])]}
                        # round(len(unique_words[c][i] & cellX_needs_review[r]) / len(cellX_needs_review[r])
                    elif i not in autoCat_needs_review[r][c+1]:
                        autoCat_needs_review[r][c+1][i] = {'unique_words': [unique_words[c][i] & cellX_needs_review[r], sum([countData[c][word][i] for word in unique_words[c][i] & cellX_needs_review[r]])]}
    """
    metaData[_needs_review] will hold meta data for various algorithms.
    This container will be used to optimize algorithm selection and performance.
    """
    metaData['unique_words'] = unique_words

    time_stamps[h+'-unique_words'] = time.time()
    print('Applied unique keywords classifier.\t', time.ctime(time_stamps[h+'-unique_words']))

    # RUN "Keyword-Ratio"

    # RUN "Inverted-Ratio"
    cellY = []
    cx_set = set()
    nr_set = set()
    for i in cellX:
        for j in i:
            cx_set.add(j)
    for i in cellX_needs_review:
        for j in i:
            nr_set.add(j)
    # find words that are unique to the new ASINs
    nw_set = nr_set - cx_set

    for c in range(len(cols)):
        print("ir_class",time.time(),"\tCol: ",c)
        icnt = 0
        cellY.append([])
        for r in range(len(cellX_needs_review)):
            if not len(cellX_needs_review[r]) > 1:
                # print(cellX_needs_review[r])
                icnt += 1
                continue
            # You have to clear the numerator and denominator values from ASIN to ASIN
            # The results from one ASIN should not affect any other, so they must be cleared.
            numeratorCat = {}
            denominatorCat = {}
            tempKeyRatios = {}
            tempKeyTie = {}
            maxcnt = 0
            # this will grab the word associated with the highest maxKey from ALL the words in the string
            # The hope is that this will help to to determine a good candidate for a unique "combo string"
            # for a given categorization.
            maxWord = 0
            maxWordVal = 0
            maxWordCount = 0
            minWord = 0
            minWordVal = 0

            cellY[-1].append([[]])

            emptykeys = 0
            emptywords = 0
            for word in cellX_needs_review[r]:
                # In here we need to make sure we are only calculating on relevant categorizations
                # i.e. if this is a child element then you have to make sure that the categorizations stem directly from the parent
                # (e.g. Cookies: chocolate chip, butter, all other  <+>  Crackers: graham, saltine, fun shape)
                nw = True
                if word in countData[c]:
                    for k1 in countData[c][word].keys():
                        sumVal += countData[c][word][k1]
                        if maxVal == countData[c][word][k1]:
                            maxcnt += 1
                        if maxVal < countData[c][word][k1]:
                            maxcnt = 1
                            maxVal = countData[c][word][k1]
                            maxKey = k1
                        if maxWordVal == countData[c][word][k1]:
                            maxWordCount += 1
                        if maxWordVal < countData[c][word][k1]:
                            maxWordCount = 1
                            maxWordVal = countData[c][word][k1]
                            maxWord = word
                    if minWordVal > sumVal:
                        minWord = word
                        minWordVal = sumVal
                    if maxcnt == 1:
                        temp = []
                        temp.append(word)
                        temp.append(round(maxVal / sumVal, 5))
                        temp.append(maxKey)
                        temp.append(maxVal)
                        cellY[-1][-1][-1].append(temp)
                        if maxVal / sumVal >= pValue:
                            if maxKey in numeratorCat:
                                numeratorCat[maxKey] += 1
                            else:
                                numeratorCat[maxKey] = 1
                        if maxKey in denominatorCat:
                            denominatorCat[maxKey] += 1
                        else:
                            denominatorCat[maxKey] = 1
                else:
                    temp = []
                    temp.append(catalogRef_needs_review[icnt][0])
                    temp.append(catalogRef_needs_review[icnt][-1])
                    if temp not in new_words:
                        new_words.append(temp)
                sumVal = 0
                maxcnt = 0
                maxVal = 0
                maxKey = 0
            cellY[-1][-1].append(numeratorCat)
            cellY[-1][-1].append(denominatorCat)
            icnt += 1

    # Start assigning categorizations for inverted ratio
    flag_bucket = {}
    for i in range(len(cellY)):
        for j in range(len(cellY[i])):
            if len(cellY[i][j][0]) == 0:
                continue
            cy = cellY[i][j][0]
            num = cellY[i][j][1]
            den = cellY[i][j][2]

            # if len(cy) != len(cellX_needs_review[j]):
            flag_nw_set = set()
            for ii in cellX_needs_review[j]:
                if len(ii.split()) > 1:
                    flag_nw_set.add(ii)
            if len(set((nw_set & cellX_needs_review[j]) - flag_nw_set)) > 0:
                # flag_cat_words = set()
                # for w in cy:
                #     flag_cat_words.add(w[0])
                if autoCat_needs_review[j][i+1] is None:
                    autoCat_needs_review[j][i+1] = {}
                if 'flag' in autoCat_needs_review[j][i+1]:
                    autoCat_needs_review[j][i+1]['flag'].add('new_word >> %s' % ((nw_set & cellX_needs_review[j]) - flag_nw_set))
                else:
                    autoCat_needs_review[j][i+1]['flag'] = set()
                    autoCat_needs_review[j][i+1]['flag'].add('new_word >> %s' % ((nw_set & cellX_needs_review[j]) - flag_nw_set))
                    flag_bucket[autoCat_needs_review[j][0]] = autoCat_needs_review[j][i+1]['flag']

            multiple_max_count = False

            # Here we basically set the logic to say 'Take the key with the highest ratio value'
            mWord = cy[0][0]
            maxVal = cy[0][1]
            maxKey = cy[0][2]
            maxcnt = 1
            for k in cy:
                if maxVal < k[1]:
                    mWord = k[0]
                    maxVal = k[1]
                    maxKey = k[2]
                    maxcnt = 1
                    multiple_max_count = False
                elif maxVal == k[1] and maxKey != k[2]:
                    maxcnt += 1
                    multiple_max_count = True
            # Here's where more customized logic may come into play - only needed when there is a conflict between two keys
            # If there is a tie for highest ratio value, take the key with the highest num/den ratio
            temp = []
            if multiple_max_count:
                maxcnt = 0
                maxVal = 0
                maxKey = 0
                for key in den:
                    if key != '':
                        if key in num:
                            tempKeyRatios[key] = num[key] / den[key]
                for key in tempKeyRatios:
                    if maxVal == 0:
                        maxVal = tempKeyRatios[key]
                    if maxVal == tempKeyRatios[key] and maxKey != key:
                        maxcnt += 1
                        maxKey = key
                        tempKeyTie[key] = None
                    if maxVal < tempKeyRatios[key]:
                        maxcnt = 1
                        maxVal = tempKeyRatios[key]
                        maxKey = key
                        if maxVal > 1:
                            print(i, j)
                            print(cellY[i][j])
                            print('maxVal = %s  maxKey = %s' % (maxVal, maxKey))
                        tempKeyTie.clear()
                if maxcnt == 1:
                    if autoCat_needs_review[j][i+1] is not None:
                        if maxKey in autoCat_needs_review[j][i+1]:
                            autoCat_needs_review[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
                        else:
                            autoCat_needs_review[j][i+1][maxKey] = {}
                            autoCat_needs_review[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
                # If there is a tie, the highest individual keyword-key ratio is selected
                if maxcnt > 1:
                    tiebreaker = 0
                    maxKey = 0
                    maxVal = 0
                    for tiekey in tempKeyTie:
                        for x in cy:
                            if tiekey == x[2]:
                                tiebreaker += 1
                        if maxVal < tiebreaker and maxKey != tiekey:
                            maxKey = tiekey
                            maxVal = tiebreaker
                        elif maxVal == tiebreaker and maxKey != tiekey:
                            maxKey = None
                        tiebreaker = 0
                    if autoCat_needs_review[j][i+1] is not None:
                        if maxKey in autoCat_needs_review[j][i+1]:
                            autoCat_needs_review[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
                        else:
                            autoCat_needs_review[j][i+1][maxKey] = {}
                            autoCat_needs_review[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
            else:
                if autoCat_needs_review[j][i+1] is not None:
                    if maxKey in autoCat_needs_review[j][i+1]:
                        autoCat_needs_review[j][i+1][maxKey]['inverted_ratio'] = [{mWord}, maxVal]
                    else:
                        autoCat_needs_review[j][i+1][maxKey] = {}
                        autoCat_needs_review[j][i+1][maxKey]['inverted_ratio'] = [{mWord}, maxVal]

    time_stamps[h+'-inverted_ratio'] = time.time()
    print('Applied inverted ratio classifier.\t', time.ctime(time_stamps[h+'-inverted_ratio']))


# ######################################################################################################################
#   Time to choose which algorithm to apply to which columns
# ######################################################################################################################
    # print('Running Algorithm checker')
# # RUN unique_words
#     for r in range(len(catalogRef)):
#         for c in range(len(countData)):
#             # remember that 'i' is the calssification (e.g. 'Z-EXCLUDE', 'N-NOT TRACKED' etc.) - not a keyword ('ball', 'spray', etc.)
#             for i in unique_words[c].keys():
#                 if len(unique_words[c][i] & cellX[r]) > 0:
#                     # catalogRef[r][c+1] = i
#                     if autoCat[r][c+1] is None:
#                         # Every automated decision option is recorded in autoCat[_needs_review]. A dictionary after the form:
#                         #       {<classification> : {<method of classification used> : <meta data about that decision>}}
#                         #   Here, in unique_words, the meta data includes a list containing (a)the inner join set of the flagged words for the automated decision and the title of the item and (b)the ratio of words in the joined set / total words in the title.
#                         autoCat[r][c+1] = {}
#                         autoCat[r][c+1][i] = {'unique_words': [unique_words[c][i] & cellX[r], round(len(unique_words[c][i] & cellX[r]) / len(cellX[r]), 5)]}
#                     elif i not in autoCat[r][c+1]:
#                         autoCat[r][c+1][i] = {'unique_words': [unique_words[c][i] & cellX[r], round(len(unique_words[c][i] & cellX[r]) / len(cellX[r]), 5)]}
# # RUN "Inverted-Ratio"
#     cellY2 = []
#     for c in range(len(cols)):
#         icnt = 0
#         cellY2.append([])
#         for r in range(len(cellX)):
#             if not len(cellX[r]) > 1:
#                 # print(cellX[r])
#                 icnt += 1
#                 continue
#             # You have to clear the numerator and denominator values from ASIN to ASIN
#             # The results from one ASIN should not affect any other, so they must be cleared.
#             numeratorCat = {}
#             denominatorCat = {}
#             tempKeyRatios = {}
#             tempKeyTie = {}
#             maxcnt = 0
#             # this will grab the word associated with the highest maxKey from ALL the words in the string
#             # The hope is that this will help to to determine a good candidate for a unique "combo string"
#             # for a given categorization.
#             maxWord = 0
#             maxWordVal = 0
#             maxWordCount = 0
#             minWord = 0
#             minWordVal = 0

#             cellY2[-1].append([[]])

#             emptykeys = 0
#             emptywords = 0
#             for word in cellX[r]:
#                 # In here we need to make sure we are only calculating on relevant categorizations
#                 # i.e. if this is a child element then you have to make sure that the categorizations stem directly from the parent
#                 # (e.g. Cookies: chocolate chip, butter, all other  <+>  Crackers: graham, saltine, fun shape)
#                 nw = True
#                 if word in countData[c]:
#                     for k1 in countData[c][word].keys():
#                         sumVal += countData[c][word][k1]
#                         if maxVal == countData[c][word][k1]:
#                             maxcnt += 1
#                         if maxVal < countData[c][word][k1]:
#                             maxcnt = 1
#                             maxVal = countData[c][word][k1]
#                             maxKey = k1
#                         if maxWordVal == countData[c][word][k1]:
#                             maxWordCount += 1
#                         if maxWordVal < countData[c][word][k1]:
#                             maxWordCount = 1
#                             maxWordVal = countData[c][word][k1]
#                             maxWord = word
#                     if minWordVal > sumVal:
#                         minWord = word
#                         minWordVal = sumVal
#                     if maxcnt == 1:
#                         temp = []
#                         temp.append(word)
#                         temp.append(round(maxVal / sumVal, 5))
#                         temp.append(maxKey)
#                         temp.append(maxVal)
#                         cellY2[-1][-1][-1].append(temp)
#                         if maxVal / sumVal >= pValue:
#                             if maxKey in numeratorCat:
#                                 numeratorCat[maxKey] += 1
#                             else:
#                                 numeratorCat[maxKey] = 1
#                         if maxKey in denominatorCat:
#                             denominatorCat[maxKey] += 1
#                         else:
#                             denominatorCat[maxKey] = 1
#                 else:
#                     temp = []
#                     temp.append(catalogRef[icnt][0])
#                     temp.append(catalogRef[icnt][-1])
#                     if temp not in new_words:
#                         new_words.append(temp)
#                 sumVal = 0
#                 maxcnt = 0
#                 maxVal = 0
#                 maxKey = 0
#             cellY2[-1][-1].append(numeratorCat)
#             cellY2[-1][-1].append(denominatorCat)
#             icnt += 1

#     # Start assigning categorizations for inverted ratio
#     for i in range(len(cellY2)):
#         for j in range(len(cellY2[i])):
#             if len(cellY2[i][j][0]) == 0:
#                 continue
#             cy = cellY2[i][j][0]
#             num = cellY2[i][j][1]
#             den = cellY2[i][j][2]

#             # if len(cy) != len(cellX[j]):
#             if len(set(nw_set & cellX[j])) > 0:
#                 # flag_cat_words = set()
#                 # for w in cy:
#                 #     flag_cat_words.add(w[0])
#                 if autoCat[j][i+1] is None:
#                     autoCat[j][i+1] = {}
#                 if 'flag' in autoCat[j][i+1]:
#                     autoCat[j][i+1]['flag'].add('inverted_ratio: unknown_word >> %s' % (nw_set & cellX[j]))
#                 else:
#                     autoCat[j][i+1]['flag'] = set()
#                     autoCat[j][i+1]['flag'].add('inverted_ratio: unknown_word >> %s' % (nw_set & cellX[j]))

#             multiple_max_count = False

#             # Here we basically set the logic to say 'Take the key with the highest ratio value'
#             mWord = cy[0][0]
#             maxVal = cy[0][1]
#             maxKey = cy[0][2]
#             maxcnt = 1
#             for k in cy:
#                 if maxVal < k[1]:
#                     mWord = k[0]
#                     maxVal = k[1]
#                     maxKey = k[2]
#                     maxcnt = 1
#                     multiple_max_count = False
#                 elif maxVal == k[1] and maxKey != k[2]:
#                     maxcnt += 1
#                     multiple_max_count = True
#             # Here's where more customized logic may come into play - only needed when there is a conflict between two keys
#             # If there is a tie for highest ratio value, take the key with the highest num/den ratio
#             temp = []
#             if multiple_max_count:
#                 maxcnt = 0
#                 maxVal = 0
#                 maxKey = 0
#                 for key in den:
#                     if key != '':
#                         if key in num:
#                             tempKeyRatios[key] = num[key] / den[key]
#                 for key in tempKeyRatios:
#                     if maxVal == 0:
#                         maxVal = tempKeyRatios[key]
#                     if maxVal == tempKeyRatios[key] and maxKey != key:
#                         maxcnt += 1
#                         maxKey = key
#                         tempKeyTie[key] = None
#                     if maxVal < tempKeyRatios[key]:
#                         maxcnt = 1
#                         maxVal = tempKeyRatios[key]
#                         maxKey = key
#                         if maxVal > 1:
#                             print(i, j)
#                             print(cellY2[i][j])
#                             print('maxVal = %s  maxKey = %s' % (maxVal, maxKey))
#                         tempKeyTie.clear()
#                 if maxcnt == 1:
#                     if autoCat[j][i+1] is not None:
#                         if maxKey in autoCat[j][i+1]:
#                             autoCat[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
#                         else:
#                             autoCat[j][i+1][maxKey] = {}
#                             autoCat[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
#                 # If there is a tie, the highest individual keyword-key ratio is selected
#                 if maxcnt > 1:
#                     tiebreaker = 0
#                     maxKey = 0
#                     maxVal = 0
#                     for tiekey in tempKeyTie:
#                         for x in cy:
#                             if tiekey == x[2]:
#                                 tiebreaker += 1
#                         if maxVal < tiebreaker and maxKey != tiekey:
#                             maxKey = tiekey
#                             maxVal = tiebreaker
#                         elif maxVal == tiebreaker and maxKey != tiekey:
#                             maxKey = None
#                         tiebreaker = 0
#                     if autoCat[j][i+1] is not None:
#                         if maxKey in autoCat[j][i+1]:
#                             autoCat[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
#                         else:
#                             autoCat[j][i+1][maxKey] = {}
#                             autoCat[j][i+1][maxKey]['inverted_ratio_tie'] = [{'?'}, maxVal]
#             else:
#                 if autoCat[j][i+1] is not None:
#                     if maxKey in autoCat[j][i+1]:
#                         autoCat[j][i+1][maxKey]['inverted_ratio'] = [{mWord}, maxVal]
#                     else:
#                         autoCat[j][i+1][maxKey] = {}
#                         autoCat[j][i+1][maxKey]['inverted_ratio'] = [{mWord}, maxVal]


    # for r in catalogRef:
    #     if row[1][0].lower() == "y":
    #         if 'pass' in row[1].lower():
    #             row[1] = "y - passthrough".upper()
    #         else:
    #             row[1] = "y".upper()
    #     elif r[1][0].lower() == "n":
    #         r[1] = "n-not tracked category".upper()
    #         for zd in range(2, len(cols)+1):
    #             r[zd] = None
    #     elif r[1][0].lower() == "z":
    #         r[1] = "z-exclude".upper()
    #         for zd in range(2, len(cols)+1):
    #             r[zd] = None

    # options = []
    # for c in range(len(cols)):
    #     options.append(set())
    #     for r in autoCat:
    #         if type(r[c+1]) is dict:
    #             for k in r[c+1]:
    #                 if k != 'flag' and k is not None:
    #                     options[-1].add(k)

    # accuracy_tracker = []
    # for c in range(len(cols)):
    #     accuracy_tracker.append({})
    #     if c == 0:
    #         for r in range(len(autoCat)):
    #             if type(autoCat[r][c+1]) is dict:
    #                 # Clean up the options and make sure there is only one (the highest) Categorization per method
    #                 #   (e.g. if unique_words shows up in 'Y' and 'Z-EXCLUDE' options, I want to make sure I'm using the higher of the two)
    #                 pd = dict(autoCat[r][c+1])
    #                 bp = {}
    #                 for k in set(pd.keys()) & options[c]:
    #                     for kk in pd[k].keys():
    #                         if k not in bp:
    #                             bp = {}
    #                             if kk not in bp:
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                             elif bp[k][1] < pd[k][kk]:
    #                                 bp[k].pop(kk)
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                         else:
    #                             if kk not in bp:
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                             elif bp[k][1] < pd[k][kk]:
    #                                 bp[k].pop(kk)
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                 pd = bp
    #                 for k in set(pd.keys()) & options[c]:
    #                     for y in pd[k].keys():
    #                         if y not in accuracy_tracker[-1]:
    #                             accuracy_tracker[-1][y] = [0, 0, 0]
    #                             if catalogRef[r][c+1] == str(k):
    #                                 accuracy_tracker[-1][y][0] += 1
    #                             accuracy_tracker[-1][y][1] += 1
    #                             accuracy_tracker[-1][y][2] = round(float(accuracy_tracker[-1][y][0] / accuracy_tracker[-1][y][1]), 5)
    #                         else:
    #                             if catalogRef[r][c+1] == str(k):
    #                                 accuracy_tracker[-1][y][0] += 1
    #                             accuracy_tracker[-1][y][1] += 1
    #                             accuracy_tracker[-1][y][2] = round(float(accuracy_tracker[-1][y][0] / accuracy_tracker[-1][y][1]), 5)
    #     else:
    #         for r in range(len(autoCat)):
    #             if type(autoCat[r][c+1]) is dict and catalogRef[r][1][0] == 'Y':
    #                 # Clean up the options and make sure there is only one (the highest) Categorization per method
    #                 #   (e.g. if unique_words shows up in 'Y' and 'Z-EXCLUDE' options, I want to make sure I'm using the higher of the two)
    #                 pd = dict(autoCat[r][c+1])
    #                 bp = {}
    #                 for k in set(pd.keys()) & options[c]:
    #                     for kk in pd[k].keys():
    #                         if k not in bp:
    #                             bp = {}
    #                             if kk not in bp:
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                             elif bp[k][1] < pd[k][kk]:
    #                                 bp[k].pop(kk)
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                         else:
    #                             if kk not in bp:
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                             elif bp[k][1] < pd[k][kk]:
    #                                 bp[k].pop(kk)
    #                                 bp[k] = {str(kk): pd[k][kk]}
    #                 pd = bp
    #                 for k in set(pd.keys()) & options[c]:
    #                     for y in pd[k].keys():
    #                         if y not in accuracy_tracker[-1]:
    #                             accuracy_tracker[-1][y] = [0, 0, 0]
    #                             if catalogRef[r][c+1] == str(k):
    #                                 accuracy_tracker[-1][y][0] += 1
    #                             accuracy_tracker[-1][y][1] += 1
    #                             accuracy_tracker[-1][y][2] = round(float(accuracy_tracker[-1][y][0] / accuracy_tracker[-1][y][1]), 5)
    #                         else:
    #                             if catalogRef[r][c+1] == str(k):
    #                                 accuracy_tracker[-1][y][0] += 1
    #                             accuracy_tracker[-1][y][1] += 1
    #                             accuracy_tracker[-1][y][asin] = round(float(accuracy_tracker[-1][y][0] / accuracy_tracker[-1][y][1]), 5)

    # metaData['accuracy_tracker'] = list(accuracy_tracker)

    # # Choose best method:
    # # So far it seems to be consistent that the unique_words method should always be preferred unless there is a tie between two unique_words options.
    # best_method = []
    # for i in accuracy_tracker:
    #     best_method.append([0, 0])
    #     for g in i:
    #         if best_method[-1][1] < i[g][-1]:
    #             best_method[-1][0] = g
    #             best_method[-1][1] = i[g][-1]

    # metaData['best_method'] = list(best_method)

# #################################################################################################################################

    # Assign categorizations
    for r in autoCat_needs_review:
        if r[0] not in output_needs_review:
            output_needs_review[r[0]] = []
        for c in range(len(cols)):
            # Test for highest unique_words value:
            maxcode = 0
            if r[c+1] is not None and type(r[c+1]) is dict:
                if maxcode < 1:
                    bp = [0, 0]
                    for k in r[c+1]:
                        if 'unique_words' in r[c+1][k]:
                            if round(bp[1], 2) < round(r[c+1][k]['unique_words'][-1], 2):
                                bp[0] = str(k) + ' | (autoCat_uw)'
                                bp[1] = float(r[c+1][k]['unique_words'][-1])
                                maxcode = 1
                            elif bp[1] == r[c+1][k]['unique_words'][-1]:
                                bp[0] = str(k) + ' | (autoCat_uw)'
                                bp[1] = float(r[c+1][k]['unique_words'][-1])
                                maxcode = -1
                if maxcode < 1:
                    bp = [0, 0]
                    for k in r[c+1]:
                        if 'inverted_ratio' in r[c+1][k]:
                            if round(bp[1], 2) < round(r[c+1][k]['inverted_ratio'][-1], 2):
                                bp[0] = str(k) + ' | (autoCat_ir1)'
                                bp[1] = float(r[c+1][k]['inverted_ratio'][-1])
                                maxcode = 2
                            elif bp[1] == r[c+1][k]['inverted_ratio'][-1]:
                                bp[0] = str(k) + ' | (autoCat_ir1)'
                                bp[1] = float(r[c+1][k]['inverted_ratio'][-1])
                                maxcode = -2
                if maxcode < 1:
                    bp = [0, 0]
                    for k in r[c+1]:
                        if 'inverted_ratio_tie' in r[c+1][k]:
                            if round(bp[1], 2) < round(r[c+1][k]['inverted_ratio_tie'][-1], 2):
                                bp[0] = str(k) + ' | (autoCat_ir2)'
                                bp[1] = float(r[c+1][k]['inverted_ratio_tie'][-1])
                                maxcode = 3
                            elif bp[1] == r[c+1][k]['inverted_ratio_tie'][-1]:
                                bp[0] = str(k) + ' | (autoCat_ir2)'
                                bp[1] = float(r[c+1][k]['inverted_ratio_tie'][-1])
                                maxcode = -3
            if maxcode > 0:
                output_needs_review[r[0]].append(bp[0])
            else:
                output_needs_review[r[0]].append('')

    # metaData['output_needs_review'] = output_needs_review
    metaData['autocat_needs_review'] = autoCat_needs_review
    metaData['output_needs_review'] = output_needs_review
    print('Writing to file...')
    # Check for errors in the hierarchy

    # This is to ensure that the output file will havethe appropriate number of columns (i.e. Needs Reviews have significantly less columns than the ASINs already in the catalog)
    maxCols = 0
    for i in catalog[1:]:
        if len(i) > maxCols:
            maxCols = len(i)
    for i in catalog:
        if len(i) < maxCols:
            for jack in range(maxCols-len(i)):
                i.append('')
        if i[asin] in output_needs_review:
            for j in range(len(cols)):
                if len(str(output_needs_review[i[asin]][j])) > 0:
                    # Here I decided to remove the tag the tells you which method was used to select the categorization so it would be easier to see when checking it in a spreadsheet
                    asdf = output_needs_review[i[asin]][j].find('|') - 1
                    i[cols[j]] = str(output_needs_review[i[asin]][j][:asdf])
    # If there is a hierarchy mismatch, find the most likely and base all other relevant categorizations on that


    # add an extra column so you can filter down to auto-categorized items
    for i in catalog:
        i.insert(0, '')
    asin += 1
    trackedCol += 1
    catalog[0][0] = 'Flags'
    for i in catalog[1:]:
        if i[asin] in output_needs_review:
            i[0] = "Auto-Cat " + i[0]
        else:
            continue
        if i[asin] in flag_bucket:
            i[0] = i[0] + "<!> " + str(flag_bucket[i[asin]])
        for j in range(len(h_cols)):
            if h_cols[j][0] == -1:
                pass
            else:
                try:
                    if hierarchy[j][i[h_cols[j][1]+1]] != i[h_cols[j][0]+1]:
                        i[h_cols[j][0]+1] = '<!>' + str(i[h_cols[j][0]+1])
                        i[0] = i[0] + "<!> HM col(%s)" % (h_cols[j][0]+1)
                except:
                    if i[trackedCol][0] != "Z" and not i[asin] in output_needs_review:
                        i[0] = i[0] + "<!> KE col(%s)" % (h_cols[j][1]+1)
    # Now let's write this sucker out to a file and go home...
    output = ''
    for row in catalog:
        for col in row:
            col = col.replace('"', "'")
            output += '"' + str(col) + '"' + ','
        output = output+'\n'
    output = output[:-1]
    outfile = results_path + "\\catalog\\TEST_%s_%s_%s.csv" % (h, master_hierarchy[h]["client_name"], time.strftime("%Y_%m_%d"))
    with open(outfile, "w") as f:
        for i in range(len(output)):
            try:
                f.write(output[i])
            except:
                pass
    print('Pickling meta data...')
    o_md = str(metaData)
    with open(results_path + "\\meta_data\\META_DATA_%s_%s.txt" % (h, time.strftime("%Y_%m_%d")), "w") as f:
        # metaout = json.dumps(metaData)
        for i in range(len(o_md)):
            try:
                f.write(o_md[i])
            except:
                pass
        # pickle.dump(metaData, f)
    with open(results_path + "\\meta_data\\META_DATA_%s_%s.pkl" % (h, time.strftime("%Y_%m_%d")), "wb") as f:
        # metaout = json.dumps(metaData)
        # f.write(str(metaData))
        pickle.dump(metaData, f)

    autoCat.clear()
    autoCat_needs_review.clear()
    catalogRef.clear()
    catalogRef_needs_review.clear()
    cellX.clear()
    cellX_needs_review.clear()
    countData.clear()
    cx_set.clear()
    flag_bucket.clear()
    hierarchy.clear()
    hierarchy_problem_children.clear()
    lev_dist_problem_children.clear()
    metaData.clear()
    new_words.clear()
    nr_set.clear()
    nw_set.clear()
    o_md = None
    output = None
    output_needs_review.clear()
    tempKeyRatios.clear()
    tempKeyTie.clear()
    unique_words.clear()

    print('Finished client #%s - %s \t %s' % (h, master_hierarchy[h]['client_name'], time.ctime(time.time())))
    return (1, outfile)
#<<<<<<<<<<<<<
